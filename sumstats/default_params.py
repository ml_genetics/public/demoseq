import time
import numpy as np


# ALL DEFAULT SETTINGS #


# Timings to kill extremely long jobs
hours_in_seconds = 3600
running_settings = {
    'min_time_rep': 2*hours_in_seconds, # if remaining time < min_time_rep simulations will stop
    'start_time': time.time(),
    'max_time': 2*24*hours_in_seconds,
}




# Useful only for when we add errors to the sequence:
# Get an appproximation of the percentage of fixed derived sites
# (because they are not present simulated data)
delta = (simu_settings['Tancestor']
         + 2 * simu_settings['longtermNe']
         - (2 * (1 - 1 / simu_settings['n_haplo']) * 2 * simu_settings['longtermNe'])
         )
sampleTreeTot = (sum([1.0/i for i in xrange(1, simu_settings['n_haplo'])])
                 * 2 * 2 * simu_settings['longtermNe'])
simu_settings['Pr_fix_der_given_fix'] = (delta * simu_settings['mu']
                                         / (1 - sampleTreeTot * simu_settings['mu']))




# IBS statistics: quantiles and numbers of individuals/haplotypes to compare
# TODO: adapt automatically size to n_haplo
sumstats_settings = {
    'compute_classical': True,
    'compute_SFS': True,
    'compute_LD': True,
    'compute_IBS': True,
    'compute_AFIBS': True,
    # IBS settings:
    "prob_list": [0.0001, 0.001, 0.01, 0.1, 0.25, 0.5, 0.75, 0.9, 0.99, 0.999, 0.9999],
    "size_list": [8, 4, 2, 1],  # diploid sample sizes, 1->compute ROH #TO
    "size_list_hap": [16, 8, 4, 2],  # haploid sample sizes
    # LD settings:
    "nb_times": 21,
    'Tmax': 130000,
    'a': 0.06,
    'per_err': 5,   # percentage for defining lower and upper bounds relative to a distance
}

# LD settings
# The older Tmax, the shorter the minimal distance at which LD is calculated
# From Boitard et al. 2016: nb_times=21; Tmax=130000; a=0.06; per_err=5
nb_times = sumstats_settings['nb_times']
Tmax = sumstats_settings['Tmax']
a = sumstats_settings['a']
per_err = sumstats_settings['per_err']  # percentage for defining lower and upper bounds relative to a distance
times = -np.ones(shape=nb_times, dtype='float')
for i in range(nb_times):
    times[i] = (np.exp(np.log(1+a*Tmax)*i/(nb_times-1))-1)/a
# Define bins (of distance) for which to LD stats will be computed
interval_list = []
for i in range(nb_times-1):
    t = (times[i+1]+times[i])/2
    d = 10**8/(2*t)
    if d <= simu_settings['L']:
        interval_list.append([d-per_err*d/100, d+per_err*d/100])
t = Tmax + times[nb_times-1] - times[nb_times-2]
d = 10**8 / (2*t)
interval_list.append([d-per_err*d/100, d+per_err*d/100])

sumstats_settings['times'] = times
sumstats_settings['interval_list'] = interval_list  # intervals for distance bins for LD

# when nind=10 nhap=20
# size_list=[5,4,2,1] # diploid sample sizes, 1->compute ROH
# size_list_hap=[10,8,4,2] # haploid sample sizes

# Fixed errors
# error_rates=[0,1e-06,1e-05]



