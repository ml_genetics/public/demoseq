AFIBS statistics are linked to the length of haplotypes shared by all individuals carrying a given a mutation. 
- See [afibs_minimal](https://gitlab.inria.fr/ml_genetics/public/demoseq/blob/master/sumstats/afibs_minimal.ipynb) notebook
- Read  [Theunert et al. 2012](https://academic-oup-com/mbe/article/29/12/3653/1005050) for a complete description of AFIBS statistics
- The implementation is based on the positional Burrows-Wheeler Transform algorithm proposed by [Durbin 2012](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC3998136/) for efficient haplotype matching
