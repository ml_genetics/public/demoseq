
AFIBS statistics are linked to the length of haplotypes shared by all individuals carrying a given a mutation.    
Read  [Theunert et al. 2012](https://academic-oup-com/mbe/article/29/12/3653/1005050) for a complete description.

The implementation is based on the positional Burrows-Wheeler Transform algorithm proposed by [Durbin 2012](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC3998136/) for efficient haplotype matching


Informal Notes about the algorithm for computing AFIBS using PBWT:

AFIBS : 
At each SNP you look at all individuals carrying the derived allele and compute the length of the haplotype extending around the focal SNP and shared by ALL these individuals (see figures in Theunert et al 2012).   
This is done for all SNPs and stratified by the allele frequency at the focal SNP.

I adapted [Durbin 2012](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC3998136/) algorithm number 2.
For a given SNP k, the left bound of the haplotype shared by all "derived individuals" is basically given by the max of the divergence array. More precisely: only the subset of dk+1 corresponding to yi[k]==0 ,  ie the max of the vector e.
The maximum is updated on the fly so that there is no additional loop afterwards.

To find the right bound we start on the right side ranging from the last snp to snp 0.   
This leads to small changes compared to Durbin 2012 algo 2, in terms of index and updates (they are highlighted with a #DIFF label in the code).  Maybe there is a way to re-use the lefbound code rather than rewritting the full function for the rightbound, I did not think too hard about it (you dont want to reverse the data in a useless extra pass?)
So this algo to compute AFIBS should be in O(2*N*M) where N is the number of individuals and M the number of SNPs.
